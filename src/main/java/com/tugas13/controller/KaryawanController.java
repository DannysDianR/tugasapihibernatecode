package com.tugas13.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tugas13.dao.KaryawanDao;
import com.tugas13.model.Karyawan;
import com.tugas13.exception.*;

@RestController
public class KaryawanController {

	@Autowired
	private KaryawanDao karyawanDao;
	
	@GetMapping("/Karyawan")
	public Page<Karyawan> getKaryawan(Pageable pageable){
		return karyawanDao.findAll(pageable);
	}
	
	@PostMapping("/Karyawan")
	public Karyawan addKaryawan(@Valid @RequestBody Karyawan karyawanSave) {
		return karyawanDao.save(karyawanSave);
	}
	
	@PutMapping("/Karyawan/{KaryawanId}")
	public Karyawan updateKaryawan(@PathVariable Long KaryawanId, @Valid @RequestBody Karyawan karyawanReq) {
		return karyawanDao.findById(KaryawanId).map(karyawan->{
			karyawan.setNama(karyawanReq.getNama());
			karyawan.setAlamat(karyawanReq.getAlamat());
			karyawan.setNoHp(karyawanReq.getNoHp());
			karyawan.setResign(karyawanReq.getResign());
			return karyawanDao.save(karyawan);
		}).orElseThrow(()-> new ResourceNotFound("Data Karyawan tidak ditemukan berdasarkan Id = " + KaryawanId));
	}
	
	@DeleteMapping("/Karyawan/{KaryawanId}")
	public ResponseEntity<?> deleteKaryawan(@PathVariable Long KaryawanId){
		return karyawanDao.findById(KaryawanId).map(karyawan -> {
			karyawanDao.delete(karyawan);
			return ResponseEntity.ok().build();
		}).orElseThrow(()-> new ResourceNotFound("Data Karyawan yg ingin dihapus tidak ditemukan berdasarkan Id = " + KaryawanId));
	}
	
}
