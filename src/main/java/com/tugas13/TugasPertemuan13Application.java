package com.tugas13;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "com.tugas13")
@EntityScan("com.tugas13.model")
@EnableJpaRepositories("com.tugas13.dao")
public class TugasPertemuan13Application {

	public static void main(String[] args) {
		SpringApplication.run(TugasPertemuan13Application.class, args);
	}

}
