package com.tugas13.dao;

import com.tugas13.model.Karyawan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface KaryawanDao extends JpaRepository<Karyawan, Long>{

}
